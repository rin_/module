#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Object{
public:
virtual string getName()=0;
};

class Phone : public Object{
public:
    virtual string getName  () override{
        return "IPhone";
    }
};

class Tablet : public Object{
public:
    virtual string getName() override{
        return "Tablet";
    }
};

int main() {
    Phone a;
    Object *obj1=&a;
    Tablet b;
    Object *obj2=&b;
    cout<<obj1->getName()<<endl;
    cout<<obj2->getName()<<endl;

   vector <int> myVector(10);
   generate(myVector.begin(), myVector.end(), []() {
       static int x ;
       return x = rand() % 10 + 1;
    });
   for_each(myVector.begin(),myVector.end(),[](int v){
       cout<< v <<" ";
   });
    int input;
    cout<<"\n Please, enter value from 2 to 5\n";
    cin>>input;
    myVector.erase(std::remove_if(myVector.begin(), myVector.end(),
            [input](int x) { return x % input!=0;}), myVector.end());
    cout<<"\nTransformed vector:\n ";
    for_each(myVector.begin(),myVector.end(),[](int v){
        cout<< v <<" ";
    });
    return 0;
}